<?php

namespace {{NAMESPACE}}\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Example extends \KDA\Navigation\Models\Menu
{
    use CrudTrait;
    
}
