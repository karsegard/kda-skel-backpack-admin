<?php

namespace {{NAMESPACE}}\Http\Controllers\Admin;

use {{NAMESPACE}}\Http\Requests\NavigationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 * Class NavigationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NavigationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\{{NAMESPACE}}\Models\Navigation::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/navigation');
        CRUD::setEntityNameStrings('navigation', 'navigations');
        $this->loadPermissions('navigation');
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', config('kda.navadmin.max_depth', 2));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name')->label('Nom');
        CRUD::column('parent');
        CRUD::column('navigation_url');
       

        CRUD::addColumn([
            'type' => 'closure',
            'function' => function ($entry) {
                if (!$entry->is_navigable) {
                    return 'non';
                }
                return 'oui';
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if (!$entry->is_navigable) {
                        return 'badge badge-error';
                    }


                    return 'badge badge-success';
                },
            ],
            'name' => 'navigable'
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(NavigationRequest::class);


        $link_tab_name = trans(config('kda.navadmin.tab_advanced_links', 'kda::tab_advanced_links'));
        $display_tab_name = trans(config('kda.navadmin.tab_display', 'kda::tab_display'));
        $menu_tab_name = trans(config('kda.navadmin.tab_menu', 'kda::tab_menu'));
        $prefix = config('kda.navadmin.site_route_filter_prefix', 'site_');
        $routes = collect(Route::getRoutes()->getRoutesByName())->reduce(function ($carry, $item, $key) use ($prefix) {
            if (strpos($key, $prefix) !== false) {
                $carry[$key] = $key;
            }
            return $carry;
        }, []);


        $header_depth =  config('kda.navadmin.max_depth_header', 2);
        $footer_depth =  config('kda.navadmin.max_depth_footer', 2);
        $depth =  config('kda.navadmin.max_depth', 2);

        CRUD::field('name')
            ->label('Nom')
            ->wrapper(['class' => 'col-md-12'])
            ->hint(__('kda-nav-admin::nav.hint_name'))
            ->tab($menu_tab_name);


        if (count($routes) > 0) {
            CRUD::addField([
                'name' => 'route',
                'type' => 'select_from_array',
                'options' => $routes,
                'tab' => $link_tab_name
            ]);
        }

        CRUD::field('url')->prefix('http://'.request()->getHttpHost().'/')->hint(__('kda-nav-admin::nav.hint_url'))->tab($link_tab_name);
        CRUD::field('external_url')->prefix('http://')->hint(__('kda-nav-admin::nav.hint_external_url'))->tab($link_tab_name);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
