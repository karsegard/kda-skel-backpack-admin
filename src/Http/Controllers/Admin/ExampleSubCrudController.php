<?php

namespace {{NAMESPACE}}\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use  {{NAMESPACE}}\Models\Menu;
class NavigationSubCrudController extends NavigationCrudController
{
    use \KDA\Backpack\Subcontroller\Traits\SubCrudController;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        parent::setup();
        CRUD::setRoute(config('backpack.base.route_prefix') . '/subnavigation');

        $this->setParentController([
            'route' => 'menu',
            'clause' => 'forMenu',
            'key' => 'menu_id'
        ]);

        $controllerSettings = $this->crud->getSubControllerCurrentValues();
        $menu = $controllerSettings ? $controllerSettings['args']['menu_id'] : NULL;
        $this->menu = Menu::find($menu);
        $this->setupSubController();
    }
    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        parent::setupListOperation();
        $this->crud->setHeading('Navigation');
        $this->crud->setSubheading('pour le menu '.$this->menu->name);
        $this->crud->removeAllFilters();
       
        $this->setupSubControllerListOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        parent::setupCreateOperation();
        $this->setupSubControllerCreateOperation();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        $this->setupSubControllerUpdateOperation();

    }
      /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupReorderOperation()
    {
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 2);
        $this->setupSubControllerReorderOperation();

    }
}
