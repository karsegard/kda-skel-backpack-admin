<?php

namespace {{NAMESPACE}};

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasTranslations;
    use \KDA\Laravel\Traits\HasDynamicSidebar;

    protected $packageName='kda-nav-admin';
    protected $viewNamespace = 'kda-nav-admin';
    protected $publishViewsTo = 'vendor/kda/backpack/nav';
    protected $sidebars = [
       /* [
            'label'=>'Navigation',
            'route'=> 'navigation',
            'behavior'=>'disable',
            'icon'=>'la-route'
        ],
       */
    ];
    protected $routes = [
        'backpack/nav.php'
    ];
    protected $configs = [
        'kda/navadmin.php'=> 'kda.navadmin'
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
