<?php
Route::group([
    'namespace'  => '{{NAMESPACE}}\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    if (config('kda.navadmin.setup_routes', true)) {

        if (config('kda.navadmin.setup_navigation_route', true)) {
            Route::crud('navigation', 'NavigationCrudController');
        }
        if (config('kda.navadmin.setup_subavigation_route', true)) {
            Route::crud('subnavigation', 'NavigationSubCrudController');
        }
        if (config('kda.navadmin.setup_menu_route', true)) {
            Route::crud('menu', 'MenuCrudController');
        }
    }
});
